/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/22 18:39:16 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/11 22:20:26 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char				*ft_strtrim(char const *s)
{
	int				i;
	int				j;
	size_t			len;

	if (!s)
		return (NULL);
	len = ft_strlen(s) - 1;
	i = 0;
	j = 0;
	while (ft_isspace(s[i]) == 1)
		i++;
	if (*(s + i) == '\0')
		return (ft_strnew(0));
	while (ft_isspace(*(s + ((int)len - j))) == 1)
		j++;
	len = ft_strlen(s) - (size_t)(i + j);
	return (ft_strsub(s, (unsigned int)i, len));
}
