/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/14 19:37:25 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/18 11:48:14 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int				ft_init_size(int nb_piece)
{
	int					i;

	nb_piece = nb_piece * 4;
	i = ft_sqrt(nb_piece);
	return (i);
}

static char				**ft_create_map(size_t size)
{
	char				**map;
	size_t				i;
	size_t				j;

	if (!(map = ft_str_tabmake(size)))
		return (NULL);
	i = 0;
	while (i < size)
	{
		j = 0;
		map[i] = ft_strnew(size);
		while (j < size)
		{
			map[i][j] = '.';
			j++;
		}
		i++;
	}
	return (map);
}

char					**ft_map_grow(char **map, size_t size)
{
	char				**ret;

	ret = ft_create_map(size + 1);
	free(map);
	return (ret);
}

int						ft_nb_piece(t_struct *data)
{
	int					i;

	i = 1;
	while (data[i].tab != NULL)
	{
		i++;
	}
	return (i);
}

char					**ft_map(t_struct *data)
{
	int					size;
	char				**map;

	size = ft_nb_piece(data);
	size = ft_init_size(size);
	if (size < 4)
		size = 2;
	map = ft_create_map(size);
	return (map);
}
