/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/20 15:58:19 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/10 11:12:30 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char				*ft_itoa(int n)
{
	size_t			len;
	unsigned int	nb;
	char			*str;

	len = ft_intlen(n);
	if (!(str = ft_strnew(len)))
		return (NULL);
	nb = (unsigned int)n;
	if (n < 0)
	{
		nb = (unsigned int)-n;
		*str = '-';
	}
	else if (n == 0)
		*str = '0';
	while (len-- && nb > 0)
	{
		*(str + len) = (char)((nb % 10) + '0');
		nb /= 10;
	}
	return (str);
}
