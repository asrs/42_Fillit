/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/20 22:05:59 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/11 13:44:13 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	int			i;
	char		*str;

	if (!s)
		return (NULL);
	else if (!(str = ft_strnew(ft_strlen(s))))
		return (NULL);
	i = 0;
	while (*s)
	{
		*(str + i) = (*f)((unsigned int)i, *s);
		i++;
		s++;
	}
	*(str + i) = '\0';
	return (str);
}
