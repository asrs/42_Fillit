/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solver.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/14 21:58:54 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/18 21:54:29 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "fillit.h"

static void			ft_give_letter(char ***map, int check, int letter,
		char to_delete)
{
	int				i;
	int				j;
	char			c;

	i = 0;
	if (check == 1)
		c = ('A' + letter);
	else if (check == 0)
		c = '.';
	while (map[0][i] != NULL)
	{
		j = 0;
		while (map[0][i][j])
		{
			if (map[0][i][j] == '#' && to_delete == -1)
				map[0][i][j] = c;
			else if (letter == -1 && map[0][i][j] == 'A' + to_delete)
				map[0][i][j] = '.';
			j++;
		}
		i++;
	}
}

static int			ft_data_cpy(char **tab, char ***map, int y, int x)
{
	int				i;
	int				j;
	int				size;

	i = 0;
	size = (int)ft_strlen(map[0][y]);
	while (tab[i] != NULL)
	{
		j = 0;
		while (tab[i][j])
		{
			if (tab[i][j] == '#' && (y + i) < size && (x + j) < size &&
					map[0][y + i][x + j] == '.')
				map[0][y + i][x + j] = tab[i][j];
			else if (tab[i][j] == '#')
			{
				ft_give_letter(map, 0, 0, -1);
				return (0);
			}
			j++;
		}
		i++;
	}
	return (1);
}

static int			ft_backtracking(t_struct *data, char **map, int y, int x)
{
	int				size;

	size = (int)ft_strlen(map[0]);
	if (ft_data_cpy(data->tab, &map, y, x) == 1)
	{
		data->x = x;
		data->y = y;
		return (1);
	}
	if (x + 1 < size)
		return (ft_backtracking(data, map, y, x + 1));
	else if (y + 1 < size)
		return (ft_backtracking(data, map, y + 1, 0));
	return (0);
}

int					ft_in_square(char **map, t_struct *data,
		int nb_piece, int size)
{
	if (data->tab == NULL)
		return (1);
	if (ft_backtracking(data, map, data->y, data->x) == 1)
	{
		ft_give_letter(&map, 1, nb_piece, -1);
		if (ft_in_square(map, data + 1, nb_piece + 1, size) == 1)
			return (1);
		else
		{
			data[1].x = 0;
			data[1].y = 0;
			ft_give_letter(&map, 1, -1, nb_piece);
			if (data->x + 1 < size)
				data->x++;
			else if (data->x + 1 == size && data->y + 1 < size)
			{
				data->x = 0;
				data->y++;
			}
			else
				return (0);
			return (ft_in_square(map, data, nb_piece, size));
		}
	}
	return (0);
}

void				ft_solver(t_struct *data, char **map)
{
	t_struct		*tmp;
	int				nb_piece;
	int				size;

	size = ft_strlen(map[0]);
	nb_piece = ft_nb_piece(data);
	tmp = data;
	while (true)
	{
		while (tmp->tab)
		{
			tmp->x = 0;
			tmp->y = 0;
			tmp++;
		}
		tmp = data;
		if (ft_in_square(map, data, 0, size) == 1)
			break ;
		map = ft_map_grow(map, (int)ft_strlen(map[0]));
		size = ft_strlen(map[0]);
	}
	ft_str_puttab(map);
	free(map);
	free(data);
}
