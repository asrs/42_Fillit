/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/22 15:17:25 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/18 22:32:40 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strsub(char const *s, unsigned int start, size_t len)
{
	size_t		i;
	char		*dest;

	if (!s)
		return (NULL);
	else if (!(dest = ft_strnew(len)))
		return (NULL);
	i = 0;
	while (i < len && s[start + i])
	{
		*(dest + i) = s[start + i];
		i++;
	}
	return (dest);
}
